﻿using System;
using System.Collections.Generic;

namespace file_io_part1_exercises_pair
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please provide a file path");
            string filePath = Console.ReadLine();
            Console.WriteLine("Please provide a file name");
            string fileName = Console.ReadLine();
            WordCount wordCount = new WordCount();

            int lines = wordCount.GetLines(filePath, fileName);

            Console.WriteLine(lines + " number of lines");
            Console.WriteLine(wordCount.CountWords() + " words");
            Console.WriteLine(wordCount.CountSentences() + " sentences");
            Console.ReadLine();
        }
    }
}
