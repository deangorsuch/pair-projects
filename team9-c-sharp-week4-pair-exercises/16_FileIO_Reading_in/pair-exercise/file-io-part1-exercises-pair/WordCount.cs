﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace file_io_part1_exercises_pair
{
    public class WordCount
    {
        List<string> lines = new List<string>();    //Because we're going to use this more than once, made it class level

        public int GetLines(string fullyQualifiedPath, string fileName)
        {
            lines = new List<string>();
            string fullPath = Path.Combine(fullyQualifiedPath, fileName);

            if (File.Exists(fullPath))
            {

                try
                {
                    using (StreamReader sr = new StreamReader(fullPath))
                    {
                        while (!sr.EndOfStream)
                        {
                            lines.Add(sr.ReadLine());

                        }

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.InnerException);
                    Console.WriteLine(ex.StackTrace);
                }
            }
            return lines.Count;
        }

        public int CountWords()
        {
            int totalWords = 0;

            foreach (string line in lines)
            {
                string[] words = line.Split(" ");
                foreach (string word in words)
                {
                    if (word.Length > 0)
                    {
                        totalWords++;
                    }
                }
            }
            return totalWords;
        }

        public int CountSentences()
        {
            int cntPeriod = CountPunctuation(".");
            int cntExclam = CountPunctuation("!");
            int cntQuestion = CountPunctuation("?");

            //foreach (string line in lines)
            //{
            //    int charIdx = -1;
            //    do
            //    {
            //        if (charIdx < line.Length)
            //        charIdx = line.IndexOf(".", charIdx + 1);
            //        if (charIdx >= 0)
            //        {
            //            cntPeriod++;
            //        }
            //    } while (charIdx >= 0);

            //}

            return cntPeriod + cntExclam + cntQuestion;
        }

        private int CountPunctuation(string str)
        {
            int cnt = 0;
            foreach (string line in lines)
            {
                int charIdx = -1;
                do
                {
                    if (charIdx < line.Length)
                        charIdx = line.IndexOf(str, charIdx + 1);
                    if (charIdx >= 0)
                    {
                        cnt++;
                    }
                } while (charIdx >= 0);
            }
            return cnt;
        }
    }
}
