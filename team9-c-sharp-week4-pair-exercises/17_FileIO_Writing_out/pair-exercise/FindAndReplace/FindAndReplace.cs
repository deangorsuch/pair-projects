﻿using System;
using System.Collections.Generic;
using System.IO;

namespace FindAndReplace
{
    public class FindAndReplace
    {
        private List<string> contents = new List<string>();     // unprocessed text from file

        public int GetFileContents(string fileAndPath)
        {
            contents = new List<string>();

            if (File.Exists(fileAndPath))
            {
                try
                {
                    using (StreamReader sr = new StreamReader(fileAndPath))
                    {
                        while (!sr.EndOfStream)
                        {
                            contents.Add(sr.ReadLine());
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.InnerException);
                    Console.WriteLine(ex.StackTrace);
                }
            }

            return contents.Count;
        }

        public bool WriteNewFile(string fileAndPath, string oldText, string newText)
        {
            bool success = false;
            try
            {
                using (StreamWriter sw = new StreamWriter(fileAndPath, false))
                {
                    foreach (string str in contents)
                    {
                        sw.WriteLine(str.Replace(oldText, newText));
                    }
                }
                success = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.InnerException);
                Console.WriteLine(ex.StackTrace);
            }

            return success;
        }

    }
}
