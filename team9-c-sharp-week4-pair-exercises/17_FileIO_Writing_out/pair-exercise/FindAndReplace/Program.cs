﻿using System;
using System.IO;

namespace FindAndReplace
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileAndPath = string.Empty;
            do
            {
                Console.Write("Enter the path to the source file: ");
                string sourcePath = Console.ReadLine();

                // get the file name
                Console.Write("Enter the source file name: ");
                string sourceFileName = Console.ReadLine();

                // make the path and check existence
                fileAndPath = Path.Combine(sourcePath, sourceFileName);
                if (!File.Exists(fileAndPath))
                {
                    Console.WriteLine("File and or path does not exist. Enter again:");
                }
            } while (!File.Exists(fileAndPath));

            // get file contents
            FindAndReplace findAndReplace = new FindAndReplace();
            findAndReplace.GetFileContents(fileAndPath);

            Console.Write("Enter the path to the destination file: ");
            string destinationPath = Console.ReadLine();

            // get the file name
            Console.Write("Enter the file name: ");
            string destinationFileName = Console.ReadLine();

            string destPathFile = Path.Combine(destinationPath, destinationFileName);
            if (File.Exists(destPathFile))
            {
                Console.WriteLine(destPathFile + " already exists.  Ciao!");
            }
            else
            {
                Console.Write("Enter the phrase to replace: ");
                string searchPhrase = Console.ReadLine();

                // get the file name
                Console.Write("Enter the replacement phrase: ");
                string replacePhrase = Console.ReadLine();


                findAndReplace.WriteNewFile(destPathFile, searchPhrase, replacePhrase);
            }
            Console.WriteLine("Press enter to end.");
            Console.ReadLine();
        }
    }
}
