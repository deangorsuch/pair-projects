﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Capstone.Classes;
using System.Collections.Generic;

namespace CapstoneTests
{
    [TestClass]
    public class CustomerTest
    {
        [TestMethod]
        public void AddTooMuchMoneyTest()
        {
            Customer customer = new Customer();

            string AddedMoney = customer.AddMoney("5001");

            string responseTest = "Deposit failed. Cannot have an account balance over $5,000";
            Assert.AreEqual(responseTest, AddedMoney);
        }

        [TestMethod]
        public void AddNotEnoughMoneyTest()
        {
            Customer customer = new Customer();

            string AddedMoney = customer.AddMoney("-1");
            string responseTest = "Deposit failed. Cannot enter a negative amount. Please enter a different amount.";
            Assert.AreEqual(responseTest, AddedMoney);
        }

        [TestMethod]
        public void AddNoMoneyTest()
        {
            Customer customer = new Customer();

            string AddedMoney = customer.AddMoney("0");

            string responseTest = "Deposit successful";
            Assert.AreEqual(responseTest, AddedMoney);
        }

        [TestMethod]
        public void AddMaxMoneyTest()
        {
            Customer customer = new Customer();

            string AddedMoney = customer.AddMoney("5000");

            string responseTest = "Deposit successful";
            Assert.AreEqual(responseTest, AddedMoney);
        }

        [TestMethod]
        public void IncrementMoneyTest()
        {
            Customer customer = new Customer();

            string AddedMoney = customer.AddMoney("5000");
            AddedMoney = customer.AddMoney("1");

            string responseTest = "Deposit failed. Cannot have an account balance over $5,000";
            Assert.AreEqual(responseTest, AddedMoney);
        }

        [TestMethod]
        public void InvalidFormatMoneyTest()
        {
            Customer customer = new Customer();

            string AddedMoney = customer.AddMoney("$5,000");
            string responseTest = "Unable to parse amount: $5,000";
            Assert.AreEqual(responseTest, AddedMoney);
        }
        [TestMethod]
        public void RemoveMoneyRemovesMoney()
        {
            Customer customer = new Customer();
            CateringItem cItem = new CateringItem();
            cItem.Price = .01M;

            customer.AddMoney("5000");
            customer.RemoveMoney(cItem, 1);
            decimal balance = 4999.99M;
            Assert.AreEqual(balance, customer.AccountBalance);
        }

        [TestMethod]
        public void RemoveMoneyRemovesMoreMoney()
        {
            Customer customer = new Customer();
            CateringItem cItem = new CateringItem();
            cItem.Price = .01M;

            customer.AddMoney("5000");
            customer.RemoveMoney(cItem, 2);
            decimal balance = 4999.98M;
            Assert.AreEqual(balance, customer.AccountBalance);
        }

        [TestMethod]
        public void ReturnChangeReturnsTwoTwentyTest()
        {
            Customer customer = new Customer();
            customer.AddMoney("40");
            customer.MakeChange();
            Dictionary<string, int> testDictionary = new Dictionary<string, int>();
            testDictionary["$20s"] = 2;

            CollectionAssert.AreEqual(testDictionary, customer.Change);
        }

        [TestMethod]
        public void ReturnChangeReturnsTwoTwentyOneFiveTest()
        {
            Customer customer = new Customer();
            customer.AddMoney("45");
            customer.MakeChange();
            Dictionary<string, int> testDictionary = new Dictionary<string, int>();
            testDictionary["$20s"] = 2;
            testDictionary["$5s"] = 1;

            CollectionAssert.AreEqual(testDictionary, customer.Change);
        }

        [TestMethod]
        public void ReturnChangeReturnsCorrectBills()
        {
            Customer customer = new Customer();
            customer.AddMoney("99");
            customer.MakeChange();
            Dictionary<string, int> testDictionary = new Dictionary<string, int>();
            testDictionary["$20s"] = 4;
            testDictionary["$10s"] = 1;
            testDictionary["$5s"] = 1;
            testDictionary["$1s"] = 4;

            CollectionAssert.AreEqual(testDictionary, customer.Change);
        }

        [TestMethod]
        public void ReturnChangeReturnsCorrectChange()
        {
            Customer customer = new Customer();
            CateringItem cItem = new CateringItem();
            cItem.Price = .01M;
            customer.AddMoney("1");
            customer.RemoveMoney(cItem, 1);
            customer.MakeChange();
            Dictionary<string, int> testDictionary = new Dictionary<string, int>();
            testDictionary["Quarters"] = 3;
            testDictionary["Dimes"] = 2;
            testDictionary["Pennies"] = 4;

            CollectionAssert.AreEqual(testDictionary, customer.Change);
        }
        [TestMethod]
        public void ReturnChangeReturnsNickles()
        {
            Customer customer = new Customer();
            CateringItem cItem = new CateringItem();
            cItem.Price = .94M;
            customer.AddMoney("1");
            customer.RemoveMoney(cItem, 1);
            customer.MakeChange();
            Dictionary<string, int> testDictionary = new Dictionary<string, int>();
            testDictionary["Nickles"] = 1;
            testDictionary["Pennies"] = 1;

            CollectionAssert.AreEqual(testDictionary, customer.Change);

        }
    }
}
