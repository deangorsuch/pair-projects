﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Capstone.Classes;

namespace CapstoneTests
{
    [TestClass]
    public class CateringTests
    {

        [DataTestMethod]
        [DataRow("A1", 1, 0)]       // ok
        [DataRow("A1",40, 1)]      // not enough money
        [DataRow("A2", 51, 1)]      // too many items
        [DataRow("A5", 1, 1)]       // bad product code
        public void IsSelectionOk(string prodCode, int quantity, int expected)
        {
            Catering cat = new Catering();
            cat.StartNewCustomer();
            cat.currentCustomer.AddMoney("100");

            string result = cat.IsSelectionOk(prodCode, quantity);

            Assert.AreEqual(expected * result.Length, result.Length);
        }

    }
}
