﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Capstone.Classes;
using System.Collections.Generic;


namespace CapstoneTests
{
    [TestClass]
    public class FileAccessTests
    {
        [TestMethod]
        public void ReadFileTestGood()
        {
            List<string> fileContents = FileAccess.ReadFile(@"c:\Catering\cateringsystem.csv");
            Assert.IsNotNull(fileContents);
        }

        // Possible TODO, test for bad file name
        //[TestMethod]
        //public void ReadFileTestException(string pathAndFile)
        //{
        //    List<string> fileContents = FileAccess.ReadFile(@"c:\Catering\Nonexistant.csv");
        //    Assert.ThrowsException<System.IO.FileNotFoundException>();
        //}

        [TestMethod]
        public void AppendFileTest()
        {
            bool result = FileAccess.WriteToFile(@"c:\Catering\Log.txt", new List<string> { "This is a testing line", "And so it this line" }, false);
            Assert.AreEqual(true, result);
        }
    }
}
