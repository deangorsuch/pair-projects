﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Capstone.Classes
{
    public class UserInterface
    {

        private Catering catering;  // catering gets instantiated in the constructor
        private Customer customer;  // customer gets instantiated when Order is selected in the main menu

        public void RunInterface()
        {
            try
            {
                catering = new Catering();
           }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine();
            }
            bool done = false;

            PrintMainMenu();
            string selection = Console.ReadLine();
            while (!done)
            {
                switch (selection)
                {
                    case "1":
                        DisplayInventory();
                        break;
                    case "2":
                        customer = catering.StartNewCustomer();
                        PrintOrderMenu();
                        NavigateOrder();
                        break;
                    case "3":
                        catering.MakeTotalSalesReport();
                        done = true;
                        return;
                    default:
                        Console.WriteLine();
                        Console.WriteLine("Please make a valid selection.");
                        Console.WriteLine();
                        break;
                }
                PrintMainMenu();
                selection = Console.ReadLine();
            }
        }

        private void PrintMainMenu()
        {
            Console.WriteLine("(1) Display Catering Items");
            Console.WriteLine("(2) Order");
            Console.WriteLine("(3) Quit");
            return;
        }

        private void PrintOrderMenu()
        {
            Console.WriteLine("(1) Add Money");
            Console.WriteLine("(2) Select Products");
            Console.WriteLine("(3) Complete Transaction");
            Console.WriteLine($"Current Account Balance: ${customer.AccountBalance}");
        }

        private void NavigateOrder()
        {
            string selection = Console.ReadLine();
            bool done = false;
            while (done != true)
            {
                switch (selection)
                {
                    case "1":
                        Console.WriteLine("Please enter the dollar amount you want to add (up to $5,000).");
                        AddToAccountBalance();
                        break;
                    case "2":
                        SelectProduct();
                        break;
                    case "3":
                        ReturnChange();
                        EndTransaction();
                        done = true;
                        return;
                    default:
                        Console.WriteLine();
                        Console.WriteLine("Please make a valid selection.");
                        Console.WriteLine();
                        break;
                }
                PrintOrderMenu();
                selection = Console.ReadLine();
            }
        }

        public void DisplayInventory()
        {
            Console.WriteLine();
            foreach (CateringItem cItem in catering.cateringItems)
            {
                Console.WriteLine(cItem.ToString());
            }
            Console.WriteLine();
            return;
        }

        public void AddToAccountBalance()
        {
            string amountToAdd = Console.ReadLine();
            Console.WriteLine(customer.AddMoney(amountToAdd));

        }

        public void SelectProduct()
        {
            DisplayInventory();
            Console.WriteLine();
            Console.Write("Enter Product Code: ");
            string prodCode = Console.ReadLine().ToUpper();

            Console.Write("Enter desired quantity: ");
            string strQuantity = Console.ReadLine();
            int quantity = 0;
            if (!int.TryParse(strQuantity, out quantity))
            {
                Console.WriteLine("Unable to parse quantity: " + strQuantity);
                return;
            }

            string selectionMsg = catering.IsSelectionOk(prodCode, quantity);
            if (selectionMsg.Length > 0)
            {   // zero length string for success, else display here and return
                Console.WriteLine(selectionMsg);
                return;
            }

            // decrement catering inventory, add to customer order, update account balance, log transaction
            CateringItem ci = catering.DecrementItemQuantity(prodCode, quantity);
            ci = customer.AddOrderItem(ci, quantity);
            customer.RemoveMoney(ci, quantity);
            FileAccess.ToLog(DateTime.Now + " " + ci.Quantity + " " + ci.ProductName + " " + ci.ProductCode
                + $" {ci.Price * ci.Quantity:C2}" + $" {customer.AccountBalance:C2}");
            return;
        }

        public void ReturnChange()
        {
            customer.MakeChange();
            foreach (KeyValuePair<string, int> kvp in customer.Change)
            {
                Console.WriteLine($"Returning {kvp.Value} {kvp.Key}");
            }
        }

        public void EndTransaction()
        {
            Console.WriteLine(customer.CompleteTransaction());
        }
    }

}
