﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Capstone.Classes
{
    public class CateringItem
    {
        public string ProductCode { get; set; } = "";
        public string ProductName { get; set; } = "";
        public decimal Price { get; set; } = 0;
        public string ProductType { get; set; } = "";
        public int Quantity { get; set; } = 50;

        public CateringItem()
        {

        }

        public CateringItem(string rawInfo, int initialQuantity)
        {
            string[] itemsToSell = rawInfo.Split('|');
            this.ProductCode = itemsToSell[0];
            this.ProductName = itemsToSell[1];
            this.Price = decimal.Parse(itemsToSell[2]);
            this.ProductType = itemsToSell[3];
            this.Quantity = initialQuantity;
        }

        public override string ToString()
        {
            if (Quantity > 0)
            {
                return ProductCode.PadRight(5)
                    + $"({Quantity.ToString()})".PadRight(9)
                    + ProductName.PadLeft(22)
                    + $"{Price:C2}".PadLeft(10)
                    + productTypeToDescr(ProductType).PadLeft(15);
            }
            else
            {   // item quantity = 0, SOLD OUT
                return ProductCode.PadRight(5)
                    + "SOLD OUT "
                    + ProductName.PadLeft(22)
                    + $"{Price:C2}".PadLeft(10)
                    + productTypeToDescr(ProductType).PadLeft(15);
            }
        }

        public string productTypeToDescr(string type)
        {
            string retString = string.Empty;
            if (type == "A")
            {
                retString = "Appetizer";
            }
            else if (type == "B")
            {
                retString = "Beverage";
            }
            else if (type == "D")
            {
                retString = "Dessert";
            }
            else if (type == "E")
            {
                retString = "Entree";
            }
            return retString;
        }

        public CateringItem MakeCopy()
        {
            CateringItem copy = new CateringItem();
            copy.ProductCode = this.ProductCode;
            copy.ProductName = this.ProductName;
            copy.Price = this.Price;
            copy.ProductType = this.ProductType;
            copy.Quantity = this.Quantity;

            return copy;
        }
    }
}
