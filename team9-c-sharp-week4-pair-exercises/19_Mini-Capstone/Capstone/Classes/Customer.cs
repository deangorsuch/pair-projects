﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Capstone.Classes
{
    public class Customer
    {
        public decimal AccountBalance { get; private set; } = 0.00M;
        public Dictionary<string, int> Change { get; set; } = new Dictionary<string, int>();
        public List<CateringItem> OrderedItems { get; private set; } = new List<CateringItem>();

        public string AddMoney(string amountToAdd)
        {
            string retString = string.Empty;
            int amount = 0;

            if (!int.TryParse(amountToAdd, out amount))
            {   // Unable to parse
                return "Unable to parse amount: " + amountToAdd;
            }

            if (amount < 0)
            {   //negative
                retString = "Deposit failed. Cannot enter a negative amount. Please enter a different amount.";
            }
            else if ((AccountBalance + amount) > 5000)
            {
                //Account goes over $5000
                retString = "Deposit failed. Cannot have an account balance over $5,000";
            }
            else
            {   // just right
                AccountBalance += amount;
                retString = "Deposit successful";
            }
            FileAccess.ToLog($" ADD MONEY: ${amountToAdd}.00 ", AccountBalance);
            return retString;
        }

        public decimal RemoveMoney(CateringItem cItem, int quantity)
        {
            decimal amountToRemove = cItem.Price * quantity;
            AccountBalance -= amountToRemove;
            return AccountBalance;
        }

        public CateringItem AddOrderItem(CateringItem cItem, int quantity)
        {
            cItem.Quantity = quantity;

            OrderedItems.Add(cItem);
            return cItem;
        }

        public Dictionary<string, int> MakeChange()
        {
            decimal oldBalance = AccountBalance;
            Withdraw("$20s", 20);
            Withdraw("$10s", 10);
            Withdraw("$5s", 5);
            Withdraw("$1s", 1);
            Withdraw("Quarters", .25M);
            Withdraw("Dimes", .10M);
            Withdraw("Nickles", .05M);
            Withdraw("Pennies", .01M);

            FileAccess.ToLog($" GIVE CHANGE: ${oldBalance} ", AccountBalance);
            return Change;
        }

        public void Withdraw(string tenderedName, decimal tenderedValue)
        {
            int currentCount = 0;
            if (AccountBalance >= tenderedValue)
            {
                Change.Add(tenderedName, 0);
                while (AccountBalance >= tenderedValue)
                {
                    AccountBalance -= tenderedValue;
                    currentCount++;
                    Change[tenderedName] = currentCount;
                }
            }
        }

        public string CompleteTransaction()
        {
            StringBuilder itemsOrdered = new StringBuilder();
            decimal runningTotal = 0.00M;
            foreach (CateringItem cItem in OrderedItems)
            {
                decimal thisTotal = cItem.Price * cItem.Quantity;
                itemsOrdered.Append(cItem.Quantity + " ");
                itemsOrdered.Append(cItem.productTypeToDescr(cItem.ProductType) + " ");
                itemsOrdered.Append(cItem.ProductName + " ");
                itemsOrdered.Append($"{cItem.Price:C2} ");
                itemsOrdered.AppendLine($"{thisTotal:C2}");
                runningTotal += thisTotal;
            }
            itemsOrdered.AppendLine("Total: " + $"{runningTotal:C2}");
            return itemsOrdered.ToString();
        }

    }
}
