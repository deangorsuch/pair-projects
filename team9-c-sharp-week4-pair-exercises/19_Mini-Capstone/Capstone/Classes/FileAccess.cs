﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Capstone.Classes
{
    public static class FileAccess
    {
        // This class should contain any and all details of access to files

        public static List<string> ReadFile(string fileAndPath)
        {
            List<string> contents = new List<string>();

            try
            {
                using (StreamReader sr = new StreamReader(fileAndPath))
                {
                    while (!sr.EndOfStream)
                    {
                        contents.Add(sr.ReadLine());
                    }
                }

            }
            catch (Exception ex)
            {
                contents.Insert(0, ex.StackTrace);
                contents.Insert(0, ex.InnerException.ToString());
                contents.Insert(0, ex.Message);
                contents.Insert(0, "EXCEPTION !!!");
                throw ex;
            }

            return contents;
        }

        public static bool WriteToFile(string fileAndPath, List<string> contents, bool append)
        {
            bool success = false;
            try
            {
                using (StreamWriter sw = new StreamWriter(fileAndPath, append))
                {
                    foreach (string str in contents)
                    {
                        sw.WriteLine(str);
                    }
                }
                success = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return success;
        }

        public static void ToLog(string content)
        {

            try
            {
                using (StreamWriter sw = new StreamWriter(@"C:\Catering\Log.txt", true))
                {
                    sw.WriteLine(content);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void ToLog(string action, decimal accountBalance)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(@"C:\Catering\Log.txt", true))
                {
                        sw.WriteLine(DateTime.Now + action + "$"+ accountBalance.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
