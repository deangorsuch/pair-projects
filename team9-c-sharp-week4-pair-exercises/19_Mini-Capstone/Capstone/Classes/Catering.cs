﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Capstone.Classes
{
    public class Catering
    {

        public List<CateringItem> cateringItems = new List<CateringItem>();
        public Customer currentCustomer;

        public Catering()
        {
            string pathFileCateringInput = Path.Combine(@"C:\Catering", "cateringsystem.csv");
            List<string> rawCateringFile = new List<string>();
            try
            {
                rawCateringFile = FileAccess.ReadFile(pathFileCateringInput);
            }
            catch (Exception ex)
            {
                FileAccess.ToLog(ex.Message);
                FileAccess.ToLog(ex.InnerException.ToString());
                FileAccess.ToLog(ex.StackTrace);
                throw new System.ArgumentException("Failed to read " + pathFileCateringInput);
            }

            foreach (string cateringInfo in rawCateringFile)
            {
                cateringItems.Add(new CateringItem(cateringInfo, 50));
            }
        }

        public Customer StartNewCustomer()
        {
            currentCustomer = new Customer();
            return currentCustomer;
        }

        public string IsSelectionOk(string prodCode, int quantity)
        {
            string retString = string.Empty;

            // find index of matching product code
            int itemIndex = cateringItems.FindIndex(ci => ci.ProductCode == prodCode);

            // no matching Product Type
            if (itemIndex < 0)
            {
                return "No product with code " + prodCode;
            }

            // negative amount requested
            if (quantity < 0)
            {
                return "The quantity must be a positive amount";
            }

            // sold out of an item
            if (cateringItems[itemIndex].Quantity == 0)
            {
                return cateringItems[itemIndex].ProductName + " is sold out";
            }

            // too many requested
            if (cateringItems[itemIndex].Quantity < quantity)
            {
                return quantity + " items requested is more than the "
                    + cateringItems[itemIndex].Quantity + " available";
            }

            // not enough money
            if (quantity * cateringItems[itemIndex].Price > currentCustomer.AccountBalance)
            {
                return $"Request comes to {quantity * cateringItems[itemIndex].Price:C2} which is more than the {currentCustomer.AccountBalance:C2} available";
            }

            return retString;
        }

        public CateringItem DecrementItemQuantity(string prodCode, int quantity)
        {   // Note: the string imputs are already vetted in IsSelectionOk
            int itemIndex = cateringItems.FindIndex(ci => ci.ProductCode == prodCode);
            cateringItems[itemIndex].Quantity -= quantity;
            return cateringItems[itemIndex].MakeCopy();
        }

        public bool MakeTotalSalesReport()
        {
            decimal totalSales = 0M;

            List<string> salesContents = new List<string>();
            if (System.IO.File.Exists(@"C:\Catering\TotalSales.rpt"))
            {
                salesContents = FileAccess.ReadFile(@"C:\Catering\TotalSales.rpt");
            }

            // iterate through old sales and update with this sessions sales
            List<string> newSales = new List<string>();
            foreach (string salesRow in salesContents)
            {
                if (!salesRow.Contains("|"))
                {   // not a data row
                    break;
                }

                string[] sales = salesRow.Split("|");
                int itemIndex = cateringItems.FindIndex(ci => ci.ProductName == sales[0]);
                if (itemIndex >= 0 && cateringItems[itemIndex].Quantity < 50)
                {   // update case
                    int quantity = int.Parse(sales[1]) + (50 - cateringItems[itemIndex].Quantity);
                    decimal rowTotal = cateringItems[itemIndex].Price * quantity;
                    totalSales += rowTotal;
                    newSales.Add(sales[0] + "|" + quantity.ToString() + "|" + $"{rowTotal:C2}");
                }
                else
                {   // pass through non-updated rows
                    newSales.Add(salesRow);
                    // add to running total
                    string justAmount = sales[2].Replace("$", "");
                    decimal rowTotal = decimal.Parse(justAmount);
                    totalSales += rowTotal;
                }
            }

            // iterate through inventory and find new items
            foreach (CateringItem cItem in cateringItems)
            {
                if (cItem.Quantity < 50)
                {
                    int itemIndex = salesContents.FindIndex(sc => sc.Contains(cItem.ProductName));
                    if (itemIndex == -1)
                    {   // no match, add here
                        int quantity = 50 - cItem.Quantity;
                        decimal rowTotal = cItem.Price * quantity;
                        newSales.Add(cItem.ProductName + "|" + quantity.ToString() + "|" + $"{rowTotal:C2}");
                        totalSales += rowTotal;
                    }
                }
            }

            newSales.Add(string.Empty);
            newSales.Add("**TOTAL SALES** " + $"{totalSales:C2}");

            return FileAccess.WriteToFile(@"C:\Catering\TotalSales.rpt", newSales, false);
        }
    }
}
