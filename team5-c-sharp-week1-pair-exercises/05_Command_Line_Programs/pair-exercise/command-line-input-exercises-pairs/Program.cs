﻿using System;

namespace command_line_input_exercises_pairs
{
    class Program
    {
        /*
        Write a command line program which prompts the user for the total bill, and the amount tendered. It should then display the change required.

        C:\Users> MakeChange

        Please enter the amount of the bill: 23.65
        Please enter the amount tendered: 100.00
        The change required is 76.35
        */
        static void Main(string[] args)
        {

            string responseBill = "";
            string responseTendered = "";
                       
            Console.WriteLine("Please enter the amount of the bill (1.00, 34.56, etc.):");
            responseBill = Console.ReadLine();
            Console.WriteLine("Please enter the amount tendered (2.00, 34.56, etc.):");
            responseTendered = Console.ReadLine();
            double amountOfBill = double.Parse(responseBill);
            double amountTendered = double.Parse(responseTendered);
            double changeRequired = (amountTendered - amountOfBill);

            Console.WriteLine("The change required is " + changeRequired);
            Console.ReadLine();
           
        }
    }
}
