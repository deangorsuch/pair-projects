-- Write queries to return the following:
-- Make the following changes in the "world" database.

-- 1. Add Superman's hometown, Smallville, Kansas to the city table. The 
-- countrycode is 'USA', and population of 45001. (Yes, I looked it up on 
-- Wikipedia.)
SELECT * FROM city
WHERE district = 'Kansas'
INSERT INTO city
(name, countrycode, district, population)
VALUES ('Smallville', 'USA', 'Kansas', 45001)

-- 2. Add Kryptonese to the countrylanguage table. Kryptonese is spoken by 0.0001
-- percentage of the 'USA' population.
SELECT * FROM countrylanguage
WHERE countrycode = 'USA'
INSERT INTO countrylanguage
(countrycode, language, isofficial, percentage)
VALUES ('USA', 'Kryptonese', 0, 0.0001)
-- 3. After heated debate, "Kryptonese" was renamed to "Krypto-babble", change 
-- the appropriate record accordingly.
UPDATE countrylanguage
SET language = 'Krypto-babble'
WHERE language = 'Kryptonese'
-- 4. Set the US captial to Smallville, Kansas in the country table.
SELECT * FROM country
WHERE code = 'USA'

SELECT * FROM city WHERE name LIKE 'Smallv%'

UPDATE country
SET capital = 4080
WHERE code = 'USA'
-- 5. Delete Smallville, Kansas from the city table. (Did it succeed? Why?) No, it is being used as a foreign key in the country table.
DELETE 
FROM city
WHERE name = 'Smallville' and district = 'Kansas'
-- 6. Return the US captial to Washington.
UPDATE country
SET capital = 3813
WHERE code = 'USA'
-- 7. Delete Smallville, Kansas from the city table. (Did it succeed? Why?) Yes, it is not longer being used as a foreign key
SELECT * FROM city
WHERE district = 'Kansas'
DELETE 
FROM city
WHERE name = 'Smallville' and district = 'Kansas'
-- 8. Reverse the "is the official language" setting for all languages where the
-- country's year of independence is within the range of 1800 and 1972 
-- (exclusive). 
-- (590 rows affected)


SELECT * FROM country
BEGIN TRANSACTION;
UPDATE countrylanguage
set isofficial =
case isofficial
when 1 then 0
when 0 then 1
end
FROM country 
JOIN countrylanguage ON country.code = countrylanguage.countrycode
WHERE (indepyear > 1800 AND indepyear < 1972)

COMMIT;


-- 9. Convert population so it is expressed in 1,000s for all cities. (Round to
-- the nearest integer value greater than 0.)
-- (4079 rows affected)
SELECT population FROM city
BEGIN TRANSACTION
UPDATE city
SET population = ROUND (population / 1000, 0)
-- 10. Assuming a country's surfacearea is expressed in square miles, convert it to 
-- square meters for all countries where French is spoken by more than 20% of the 
-- population.
-- (7 rows affected)
BEGIN TRANSACTION;
UPDATE country
SET surfacearea *= 2589988
FROM countrylanguage
JOIN country ON countrylanguage.countrycode = country.code 
WHERE language = 'French' and percentage > 20
COMMIT;

