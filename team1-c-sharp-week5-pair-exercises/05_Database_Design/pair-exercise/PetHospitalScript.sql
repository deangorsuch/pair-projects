CREATE DATABASE HospitalVisit;
GO

USE HospitalVisit

CREATE TABLE PetOwner
(
pet_owner_id int IDENTITY PRIMARY KEY,
address_id int,
first_name nvarchar(10),
last_name nvarchar(10),
pet_id int
);

INSERT INTO PetOwner(address_id, first_name, last_name, pet_id)
VALUES(1, 'Richard', 'Cook', 1);

CREATE TABLE Address
(
address_id int IDENTITY PRIMARY KEY,
street_address nvarchar(60),
city nvarchar(20),
state nvarchar(2),
zip_code int,
);
INSERT INTO Address (street_address, city, state, zip_code)
VALUES('123 This Street', 'My City', 'Ontario', 12345);

CREATE TABLE Pet
(
pet_id int IDENTITY PRIMARY KEY,
pet_name nvarchar(10),
pet_age int,
pet_owner_id int,
pet_type_id int
);

INSERT INTO Pet(pet_name, pet_age, pet_owner_id, pet_type_id)
VALUES('Rover', 12, 1, 1);

CREATE TABLE PetType
(
pet_type_id int IDENTITY PRIMARY KEY,
pet_type_name nvarchar(10)
);

INSERT INTO PetType(pet_type_name)
VALUES('dog');

CREATE TABLE ProcedureCost
(
procedure_id int,
pet_type_id int,
cost int

);
INSERT INTO ProcedureCost(procedure_id, pet_type_id, cost)
VALUES(1, 1, 30.00);

CREATE TABLE PetProcedure
(
procedure_id int IDENTITY PRIMARY KEY,
procedure_name nvarchar(30)
);
INSERT INTO PetProcedure(procedure_name)
VALUES('Rabies Vaccination');

CREATE TABLE Visit
(
invoice_id int IDENTITY PRIMARY KEY,
invoice_date datetime,
pet_owner_id int,
pet_id int,
procedure_id int
);
INSERT INTO Visit(invoice_date, pet_owner_id, pet_id, procedure_id)
VALUES(2002-1-13, 1, 1, 1) ;

ALTER TABLE [dbo].[PetOwner]  WITH CHECK ADD FOREIGN KEY([pet_id])
REFERENCES [dbo].[Pet] ([pet_id]);

ALTER TABLE [dbo].[Pet]  WITH CHECK ADD FOREIGN KEY([pet_owner_id])
REFERENCES [dbo].[PetOwner] ([pet_owner_id]);

ALTER TABLE [dbo].[Pet]  WITH CHECK ADD FOREIGN KEY([pet_type_id])
REFERENCES [dbo].[PetType] ([pet_type_id]);

ALTER TABLE [dbo].[ProcedureCost]  WITH CHECK ADD FOREIGN KEY([procedure_id])
REFERENCES [dbo].[PetProcedure] ([procedure_id]);

ALTER TABLE [dbo].[ProcedureCost] WITH CHECK ADD FOREIGN KEY([pet_type_id])
REFERENCES [dbo].[PetType] ([pet_type_id]);

ALTER TABLE [dbo].[Visit] WITH CHECK ADD FOREIGN KEY([pet_owner_id])
REFERENCES [dbo].[PetOwner] ([pet_owner_id]);

ALTER TABLE [dbo].[Visit] WITH CHECK ADD FOREIGN KEY([pet_id])
REFERENCES [dbo].[Pet] ([pet_id]);

ALTER TABLE [dbo].[Visit] WITH CHECK ADD FOREIGN KEY([procedure_id])
REFERENCES [dbo].[PetProcedure] ([procedure_id]);

SELECT * FROM Pet

