﻿using SSGeek.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SSGeek.Web.DAL
{
    public class ProductSqlDAO : IProductDAO
    {
        private string connectionString;

        public ProductSqlDAO(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public Product GetProduct(int id)
        {
            Product product = new Product();
            try
            {               
               using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT * FROM products WHERE product_id = @Id", conn);
                    cmd.Parameters.AddWithValue("@Id", id);
                    SqlDataReader reader = cmd.ExecuteReader();
                    
                    while (reader.Read())
                    {                 
                        {
                            product.Id = Convert.ToInt32(reader["product_id"]);
                            product.Name = Convert.ToString(reader["name"]);
                            product.Description = Convert.ToString(reader["description"]);
                            product.Price = Convert.ToDecimal(reader["price"]);
                            product.ImageName = Convert.ToString(reader["image_name"]);
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return product;
        }
    

        public IList<Product> GetProducts()
        {
            IList<Product> products = new List<Product>();
            try
            {

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT * FROM products", conn);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Product product = new Product();
                        {
                            product.Id = Convert.ToInt32(reader["product_id"]);
                            product.Name = Convert.ToString(reader["name"]);
                            product.Description = Convert.ToString(reader["description"]);
                            product.Price = Convert.ToDecimal(reader["price"]);
                            product.ImageName = Convert.ToString(reader["image_name"]);

                            products.Add(product);
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return products;
        }
    }
}
