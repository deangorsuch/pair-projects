﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using SSGeek.Web.Models;

namespace SSGeek.Web.DAL
{
    public class ForumPostSqlDAO : IForumPostDAO
    {
        private string connectionString;

        public ForumPostSqlDAO(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public IList<ForumPost> GetAllPosts()
        {
            IList<ForumPost> forumPosts = new List<ForumPost>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT * FROM forum_post", conn);

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var post = new ForumPost()
                        {
                            UserName = Convert.ToString(reader["username"]),
                            Subject = Convert.ToString(reader["subject"]),
                            Message = Convert.ToString(reader["message"]),
                            PostDate = Convert.ToDateTime(reader["post_date"]),
                        };

                        forumPosts.Add(post);
                    }
                }
            }
            catch (SqlException ex)
            {
                throw;
            }

            return forumPosts;
        }

        public bool SaveNewPost(ForumPost post)
        {
            int posted = 0;
            bool newPost = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    string sql = $"INSERT INTO forum_post (username, subject, message, post_date) VALUES (@username, @subject, @message, @postdate);";
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@username", post.UserName);
                    cmd.Parameters.AddWithValue("@subject", post.Subject);
                    cmd.Parameters.AddWithValue("@message", post.Message);
                    cmd.Parameters.AddWithValue("@postdate", post.PostDate);

                    posted = cmd.ExecuteNonQuery();

                    if(posted>0)
                    {
                        newPost = true;
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }

            return newPost;
        }
    }
}
