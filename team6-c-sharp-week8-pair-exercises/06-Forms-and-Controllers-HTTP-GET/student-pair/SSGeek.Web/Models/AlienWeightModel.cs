﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SSGeek.Web.Models
{
    public class AlienWeightModel
    {
        public double EarthWeight { get; set; }
        public string Planet { get; set; }
        public double WeightConversion()
        {
            Dictionary<string, double> conversionToPlanetWeight = new Dictionary<string, double>()
            {
            {"Mercury", 0.38},
            {"Venus", 0.91},
            {"Mars", 0.38},
            {"Jupiter", 2.34},
            {"Saturn", 1.06},
            {"Uranus", 0.92},
            {"Neptune", 1.19}
        };
            return conversionToPlanetWeight[Planet]*EarthWeight;
        }
    }
}