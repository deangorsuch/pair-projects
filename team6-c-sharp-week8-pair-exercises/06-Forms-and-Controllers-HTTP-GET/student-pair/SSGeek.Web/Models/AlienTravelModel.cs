﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSGeek.Web.Models
{
    public class AlienTravelModel
    {
        Dictionary<string, double> modeOfTransportSpeed = new Dictionary<string, double>()
            {
            {"Walking", 3},
            {"Car", 100},
            {"Bullet Train", 200},
            {"Boeing 747", 570},
            {"Concord", 1350},
        };
        Dictionary<string, double> planetDistance = new Dictionary<string, double>()
            {
            {"Mercury", 56974146},
            {"Venus", 25724767},
            {"Mars", 48678219},
            {"Jupiter", 390674710},
            {"Saturn", 792248270},
            {"Uranus", 1692662530},
            {"Neptune", 2703959960}
            };

        public double EarthAge { get; set; }
        public string Planet { get; set; }
        public string Transportation { get; set; }

        public double TravelTimeInYears(string Planet, string Transportation)
        {
            double travelTime = planetDistance[Planet] / modeOfTransportSpeed[Transportation] / 8760;
            return travelTime;
        } 
        public double AgeAfterTravel()
        {
            double ageAfterTravel = EarthAge + TravelTimeInYears(Planet, Transportation);
                return ageAfterTravel;
        }
   
    }
}
