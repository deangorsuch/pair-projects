﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSGeek.Web.Models
{
    public class AlienAgeModel
    {
        public double EarthAge { get; set; }
        public string Planet { get; set; }
        public double AgeConversion()
        {
            Dictionary<string, double> conversionToPlanetAge = new Dictionary<string, double>()
            {
            {"Mercury", 4.15},
            {"Venus", 1.63},
            {"Mars", 0.53},
            {"Jupiter", 0.08},
            {"Saturn", 0.03},
            {"Uranus", 0.012},
            {"Neptune", 0.006}
        };
            return conversionToPlanetAge[Planet] * EarthAge;
        }
    }
}
