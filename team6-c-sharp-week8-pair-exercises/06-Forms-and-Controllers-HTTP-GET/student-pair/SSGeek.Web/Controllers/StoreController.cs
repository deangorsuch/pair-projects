﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SSGeek.Web.DAL;
using SSGeek.Web.Extensions;
using SSGeek.Web.Models;

namespace SSGeek.Web.Controllers
{
    public class StoreController : Controller
    {
        private IProductDAO productDAO;
        public StoreController(IProductDAO productDAO)
        {
            this.productDAO = productDAO;
        }

        public ActionResult Index()
        {
            IList<Product> products = productDAO.GetProducts();
            return View(products);
        }

        public ActionResult Detail(int id)
        {
            Product product = new Product();
            product = productDAO.GetProduct(id);
            return View(product);
        }

        public ActionResult ViewCart()
        {
            ShoppingCart cart = GetActiveShoppingCart();
            return View(cart);
        }

        [HttpPost]
        public ActionResult AddToCart(Product product, int quantity)
        {
            product = productDAO.GetProduct(product.Id);
            ShoppingCart cart = GetActiveShoppingCart();
            cart.AddToCart(product, quantity);
            SaveActiveShoppingCart(cart);

            return RedirectToAction("ViewCart");
        }

        private ShoppingCart GetActiveShoppingCart()
        {
            ShoppingCart cart = null;
           
            if (HttpContext.Session.Get<ShoppingCart>("Shopping_Cart") == null)
            {
                cart = new ShoppingCart();
                SaveActiveShoppingCart(cart);
            }
            else
            {
                cart = HttpContext.Session.Get<ShoppingCart>("Shopping_Cart");
            }

            return cart;
        }

        private void SaveActiveShoppingCart(ShoppingCart cart)
        {
            HttpContext.Session.Set("Shopping_Cart", cart);
        }

    }
}