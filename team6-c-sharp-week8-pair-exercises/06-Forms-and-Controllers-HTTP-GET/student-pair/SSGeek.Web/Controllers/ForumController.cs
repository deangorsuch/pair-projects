﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SSGeek.Web.DAL;
using SSGeek.Web.Extensions;
using SSGeek.Web.Models;

namespace SSGeek.Web.Controllers
{
    public class ForumController : Controller
    {
        private IForumPostDAO forumPostDao;

        public ForumController(IForumPostDAO forumPostDao)
        {
            this.forumPostDao = forumPostDao;
        }

        public IActionResult Index(ForumPost forumPost)
        {
            ViewBag.SuccessMessage = HttpContext.Session.GetString("message");
            IList<ForumPost> posts = forumPostDao.GetAllPosts();
            return View(posts);
        }

        [HttpGet]
        public IActionResult NewPost()
        {
            ForumPost forumPost = new ForumPost();
            return View(forumPost);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult NewPost(ForumPost forumPost)
        {
            if(!ModelState.IsValid)
            {
                return View(forumPost);
            }

            bool result = false;
            result = forumPostDao.SaveNewPost(forumPost);
            if(result)
            {
                HttpContext.Session.SetString("message", "Your message has been saved!");
            }

            return RedirectToAction("Index", "Forum");
        }
    }
}