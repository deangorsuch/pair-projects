﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectOrganizer.DAL;
using ProjectOrganizer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Transactions;

namespace ExercisesTest
{
    [TestClass]
    public class ProjectTest
    {
        protected string ConnectionString { get; } = "Data Source=.\\sqlexpress;Initial Catalog=EmployeeDB;Integrated Security=True";

        private TransactionScope transaction;

        [TestInitialize]
        public void Setup()
        {
            //Begin transaction
            transaction = new TransactionScope();
        }

        [TestCleanup]
        public void Cleanup()
        {
            transaction.Dispose();
        }

        [TestMethod]
        public void GetAllProjectsReturnsAddedProject()
        {
            ProjectSqlDAO dao = new ProjectSqlDAO(ConnectionString);
            Project project = new Project();
            bool result = false;

            project.Name = "test1";
            project.StartDate = Convert.ToDateTime("1/1/2019");
            project.EndDate = Convert.ToDateTime("2/2/2019");
            int startingCount = GetRowCount("project");

            dao.CreateProject(project);

            IList<Project> projectList = dao.GetAllProjects();

            foreach (Project projects in projectList)
            {
                if (projects.Name == "test1")
                {
                    result = true;
                    break;
                }
            }

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void AssignEmployeeAddOneRowToProjectEmployee()
        {
            Employee employee = new Employee();

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("INSERT INTO employee (department_id, first_name, last_name, job_title, birth_date, gender, hire_date) VALUES(1, 'Ada', 'Lovelace', 'mathperson', '1/10/2019', 'F', '1/11/2019');", conn);
                    cmd.ExecuteNonQuery();
                }
                EmployeeSqlDAO employeedao = new EmployeeSqlDAO(ConnectionString);


                employeedao.GetAllEmployees();

                IList<Employee> employeeList = employeedao.GetAllEmployees();

            }
            catch (Exception ex)
            {
                string failed = "this didn't work";
            }
            ProjectSqlDAO projectdao = new ProjectSqlDAO(ConnectionString);
            Project project = new Project();
            bool result = false;

            project.Name = "test1";
            project.StartDate = Convert.ToDateTime("1/1/2019");
            project.EndDate = Convert.ToDateTime("2/2/2019");


            projectdao.CreateProject(project);
            int startingCount = GetRowCount("project_employee");
            project.ProjectId = 1;
            employee.EmployeeId = 1;
            projectdao.AssignEmployeeToProject(project.ProjectId, employee.EmployeeId);
            int endCount = GetRowCount("project_employee");

            Assert.AreEqual(startingCount + 1, endCount);
        }

        [TestMethod]
        public void RemoveEmployeeFromProjectTest()
        {
            Employee employee = new Employee();

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("INSERT INTO employee (department_id, first_name, last_name, job_title, birth_date, gender, hire_date) VALUES(1, 'Ada', 'Lovelace', 'mathperson', '1/10/2019', 'F', '1/11/2019');", conn);
                    cmd.ExecuteNonQuery();
                }
                EmployeeSqlDAO employeedao = new EmployeeSqlDAO(ConnectionString);


                employeedao.GetAllEmployees();

                IList<Employee> employeeList = employeedao.GetAllEmployees();

            }
            catch (Exception ex)
            {
                string failed = "this didn't work";
            }
            ProjectSqlDAO projectdao = new ProjectSqlDAO(ConnectionString);
            Project project = new Project();

            project.Name = "test1";
            project.StartDate = Convert.ToDateTime("1/1/2019");
            project.EndDate = Convert.ToDateTime("2/2/2019");


            projectdao.CreateProject(project);
            int startingCount = GetRowCount("project_employee");
            project.ProjectId = 1;
            employee.EmployeeId = 1;
            projectdao.AssignEmployeeToProject(project.ProjectId, employee.EmployeeId);
            int endCount = GetRowCount("project_employee");

            Assert.AreEqual(startingCount + 1, endCount);

            projectdao.RemoveEmployeeFromProject(project.ProjectId, employee.EmployeeId);
            endCount = GetRowCount("project_employee");
            Assert.AreEqual(startingCount, endCount);
        }

        [TestMethod]
        public void CreateProjectAddsOneRow()
        {
            ProjectSqlDAO dao = new ProjectSqlDAO(ConnectionString);
            Project project = new Project();

            project.Name = "test1";
            project.StartDate = Convert.ToDateTime("1/1/2019");
            project.EndDate = Convert.ToDateTime("2/2/2019");
            int startingCount = GetRowCount("project");

            dao.CreateProject(project);

            int endingCount = GetRowCount("project");

            Assert.AreEqual(startingCount + 1, endingCount);
        }

        protected int GetRowCount(string table)
        {
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand($"SELECT COUNT(*) FROM {table}", conn);
                int count = Convert.ToInt32(cmd.ExecuteScalar());
                return count;
            }
        }
    }
}
