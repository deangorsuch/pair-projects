﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectOrganizer.DAL;
using ProjectOrganizer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Transactions;

namespace ExercisesTest
{
    [TestClass]
    public class DepartmentTest
    {
        protected string ConnectionString { get; } = "Data Source=.\\sqlexpress;Initial Catalog=EmployeeDB;Integrated Security=True";

        private TransactionScope transaction;

        [TestInitialize]
        public void Setup()
        {
            //Begin transaction
            transaction = new TransactionScope();
        }

        [TestCleanup]
        public void Cleanup()
        {
            transaction.Dispose();
        }


        [TestMethod]
        public void CreateDepartmentAddsOneRow()
        {
            DepartmentSqlDAO dao = new DepartmentSqlDAO(ConnectionString);
            Department department = new Department();

            department.Name = "test1";
            int startingCount = GetRowCount("department");

            dao.CreateDepartment(department);

            int endingCount = GetRowCount("department");

            Assert.AreEqual(startingCount + 1, endingCount);
        }

        [TestMethod]
        public void CreateDepartmentCreatesAndGetDepartmentReturnIt()
        {
            DepartmentSqlDAO dao = new DepartmentSqlDAO(ConnectionString);
            Department department = new Department();
            bool result = false;
            department.Name = "test1";
            dao.CreateDepartment(department);

            IList<Department> departmentList = dao.GetDepartments();
            
            foreach(Department departments in departmentList)
            {
                if(departments.Name == "test1")
                {
                    result = true;
                    break;
                }
            }

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void UpdateDepartmentUpdatesADepartmentAndGetsFound()
        {
            DepartmentSqlDAO dao = new DepartmentSqlDAO(ConnectionString);
            Department department = new Department();
            bool result = false;
            department.Name = "test1";
            dao.CreateDepartment(department);

            department.Id = 1;
            department.Name = "test2";
            dao.UpdateDepartment(department);

            IList<Department> departmentList = dao.GetDepartments();

            foreach (Department departments in departmentList)
            {
                if (departments.Name == "test2")
                {
                    result = true;
                    break;
                }
            }

            Assert.IsTrue(result);
        }


        protected int GetRowCount(string table)
        {
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand($"SELECT COUNT(*) FROM {table}", conn); //get # rows after add city
                int count = Convert.ToInt32(cmd.ExecuteScalar());
                return count;
            }
        }
    }
}
