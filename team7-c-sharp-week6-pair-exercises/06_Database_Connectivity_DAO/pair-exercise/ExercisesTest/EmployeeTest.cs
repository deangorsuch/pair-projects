using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectOrganizer.DAL;
using ProjectOrganizer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Transactions;

namespace ExercisesTest
{

    [TestClass]
    public class EmployeeTest
    {
        protected string ConnectionString { get; } = "Data Source=.\\sqlexpress;Initial Catalog=EmployeeDB;Integrated Security=True";

        private TransactionScope transaction;

        [TestInitialize]
        public void Setup()
        {
            //Begin transaction
            transaction = new TransactionScope();
        }

        [TestCleanup]
        public void Cleanup()
        {
            transaction.Dispose();
        }

        [TestMethod]
        public void GetAllEmployeesFindsANewEmployeeTest()
        {
            bool result = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("INSERT INTO employee (department_id, first_name, last_name, job_title, birth_date, gender, hire_date) VALUES(1, 'Ada', 'Lovelace', 'mathperson', '1/10/2019', 'F', '1/11/2019');", conn);
                    cmd.ExecuteNonQuery();
                }
                EmployeeSqlDAO dao = new EmployeeSqlDAO(ConnectionString);
                Employee employee = new Employee();


                dao.GetAllEmployees();

                IList<Employee> employeeList = dao.GetAllEmployees();

                foreach (Employee employees in employeeList)
                {
                    if (employees.FirstName == "Ada" && employees.LastName == "Lovelace")
                    {
                        result = true;
                        break;
                    }
                }

                Assert.IsTrue(result);
            }
            catch (Exception ex)
            {
                string failed = "this didn't work";
                throw;
            }

        }
        [TestMethod]
        public void SearchFindsNewEmployee()
        {
            bool result = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("INSERT INTO employee (department_id, first_name, last_name, job_title, birth_date, gender, hire_date) VALUES(1, 'Ada', 'Lovelace', 'mathperson', '1/10/2019', 'F', '1/11/2019');", conn);
                    cmd.ExecuteNonQuery();
                }
                EmployeeSqlDAO dao = new EmployeeSqlDAO(ConnectionString);
                Employee employee = new Employee();




                IList<Employee> employeeList = dao.Search("da", "ove"); ;

                foreach (Employee employees in employeeList)
                {
                    if (employees.FirstName == "Ada" && employees.LastName == "Lovelace")
                    {
                        result = true;
                        break;
                    }
                }

                Assert.IsTrue(result);
            }
            catch (Exception ex)
            {
                string failed = "this didn't work";
            }
        }

        [TestMethod]
        public void EmployeesWithoutProjectsFindsEmployeeAndThenIsAdded()
        {
            EmployeeSqlDAO employeedao = new EmployeeSqlDAO(ConnectionString);
            Employee employee = new Employee();
            int id = 0;
            bool result = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("INSERT INTO employee (first_name, last_name, job_title, birth_date, gender, hire_date) VALUES('Ada', 'Lovelace', 'mathperson', '1/10/2019', 'F', '1/11/2019');", conn);
                    cmd.ExecuteNonQuery();
                    cmd = new SqlCommand("SELECT Ident_Current;");
                    id = Convert.ToInt32(cmd.ExecuteScalar());
                }
            }
            catch (Exception ex)
            {
                string failed = "this didn't work";
            }

            ProjectSqlDAO projectdao = new ProjectSqlDAO(ConnectionString);
            Project project = new Project();

            project.Name = "test1";
            project.StartDate = Convert.ToDateTime("1/1/2019");
            project.EndDate = Convert.ToDateTime("2/2/2019");

            projectdao.CreateProject(project);

            project.ProjectId = 1;
            employee.EmployeeId = id;
            projectdao.AssignEmployeeToProject(project.ProjectId, employee.EmployeeId);

            bool employeeGone = true;

            IList<Employee> newEmployeeList = employeedao.GetEmployeesWithoutProjects();

            foreach (Employee employees in newEmployeeList)
            {
                if (employees.FirstName == "Ada" && employees.LastName == "Lovelace")
                {
                    employeeGone = false;
                    break;
                }
            }

            Assert.IsTrue(employeeGone);

        }

    }
}
