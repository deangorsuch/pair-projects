﻿using ProjectOrganizer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace ProjectOrganizer.DAL
{
    public class ProjectSqlDAO : IProjectDAO
    {
        private string connectionString;
        private string sql_GetAllProjects = "SELECT * FROM Project";
        private string sql_AssignEmployeeToProject = "INSERT INTO project_employee (project_id, employee_id) VALUES (@project_id, @employee_id)";
        private string sql_RemoveEmployeeFromProject = "DELETE FROM project_employee WHERE project_id = @project_id AND employee_id = @employee_id";
        private string sql_CreateProject = "INSERT INTO Project (name, from_date, to_date) VALUES (@project_name, @project_start, @project_end)";

        // Single Parameter Constructor
        public ProjectSqlDAO(string dbConnectionString)
        {
            connectionString = dbConnectionString;
        }

        /// <summary>
        /// Returns all projects.
        /// </summary>
        /// <returns></returns>
        public IList<Project> GetAllProjects()
        {
            IList<Project> projects = new List<Project>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql_GetAllProjects, conn))
                    {

                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            Project project = new Project();

                            project.ProjectId = Convert.ToInt32(reader["project_id"]);
                            project.Name = Convert.ToString(reader["name"]);
                            project.StartDate = Convert.ToDateTime(reader["from_date"]);
                            project.EndDate = Convert.ToDateTime(reader["to_date"]);


                            projects.Add(project);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                projects = new List<Project>();
            }

            return projects;
        }


        /// <summary>
        /// Assigns an employee to a project using their IDs.
        /// </summary>
        /// <param name="projectId">The project's id.</param>
        /// <param name="employeeId">The employee's id.</param>
        /// <returns>If it was successful.</returns>
        public bool AssignEmployeeToProject(int projectId, int employeeId)
        {
            bool result = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql_AssignEmployeeToProject, conn))
                    {

                        cmd.Parameters.AddWithValue("@project_Id", projectId);
                        cmd.Parameters.AddWithValue("@employee_Id", employeeId);

                        int count = cmd.ExecuteNonQuery();

                        if (count > 0)
                        {
                            result = true;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Removes an employee from a project.
        /// </summary>
        /// <param name="projectId">The project's id.</param>
        /// <param name="employeeId">The employee's id.</param>
        /// <returns>If it was successful.</returns>
        public bool RemoveEmployeeFromProject(int projectId, int employeeId)
        {
            bool result = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql_RemoveEmployeeFromProject, conn))
                    {

                        cmd.Parameters.AddWithValue("@project_Id", projectId);
                        cmd.Parameters.AddWithValue("@employee_Id", employeeId);

                        int count = cmd.ExecuteNonQuery();

                        if (count > 0)
                        {
                            result = true;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Creates a new project.
        /// </summary>
        /// <param name="newProject">The new project object.</param>
        /// <returns>The new id of the project.</returns>
        public int CreateProject(Project newProject)
        {
            int result = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql_CreateProject, conn))
                    {

                        cmd.Parameters.AddWithValue("@project_name", newProject.Name);
                        cmd.Parameters.AddWithValue("@project_start", newProject.StartDate);
                        cmd.Parameters.AddWithValue("@project_end", newProject.EndDate);

                        result = cmd.ExecuteNonQuery();

                    }
                }
            }
            catch (Exception ex)
            {
                result = 0;
            }

            return result;
        }
    }

}
