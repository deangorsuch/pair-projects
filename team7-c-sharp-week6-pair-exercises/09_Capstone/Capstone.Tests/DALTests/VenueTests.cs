using Capstone.DAL;
using Capstone.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Transactions;

namespace Capstone.Tests
{
    [TestClass]
    public class VenueTests
    {
        protected string ConnectionString { get; } = "Data Source=.\\sqlexpress;Initial Catalog=excelsior_venues;Integrated Security=True";

        private TransactionScope transaction;

        [TestInitialize]
        public void Setup()
        {
            transaction = new TransactionScope();
        }

        [TestCleanup]
        public void Cleanup()
        {
            transaction.Dispose();
        }
        [TestMethod]
        public void GetVenuesCanFindAddedVenue()
        {
            bool result = false;
            VenueDAO dao = new VenueDAO(ConnectionString);

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("INSERT INTO venue (name, city_id, description) VALUES('testVenue', 1, 'this is a test');", conn);
                    cmd.ExecuteNonQuery();
                }

            }
            catch (Exception ex)
            {
                string failed = "this didn't work";
            }

            List<Venue> venueList = dao.GetAllVenues();
            foreach (Venue venue in venueList)
            {
                if (venue.Name == "testVenue")
                {
                    result = true;
                    break;
                }
            }
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void GetVenueGetsASpecificVenue()
        {
            bool result = false;
            int venId = 0;
            VenueDAO dao = new VenueDAO(ConnectionString);
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("INSERT INTO venue (name, city_id, description) VALUES('testVenue', 1, 'this is a test') SELECT SCOPE_IDENTITY();", conn);
                    venId = Convert.ToInt32(cmd.ExecuteScalar());
                }

            }
            catch (Exception ex)
            {
                string failed = "this didn't work";
            }

            Venue testVenue = dao.GetSpecificVenue(venId);

            if(testVenue.Name == "testVenue")
            {
                result = true;
            }

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void GetCategoriesFindsANewCategory()
        {
            bool result = false;
            VenueDAO dao = new VenueDAO(ConnectionString);
            int catId = 0;
            int venId = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("INSERT INTO venue (name, city_id, description) VALUES('testVenue', 1, 'this is a test') SELECT SCOPE_IDENTITY();", conn);
                    venId = Convert.ToInt32(cmd.ExecuteScalar());

                    cmd = new SqlCommand("INSERT INTO category (name) VALUES('test') SELECT SCOPE_IDENTITY()", conn);
                    catId = Convert.ToInt32(cmd.ExecuteScalar());

                    cmd = new SqlCommand("INSERT INTO category_venue (venue_id, category_id) VALUES(@venid, @catid)", conn);
                    cmd.Parameters.AddWithValue("@venid", venId);
                    cmd.Parameters.AddWithValue("@catid", catId);
                    cmd.ExecuteNonQuery();
                }

            }
            catch (Exception ex)
            {
                string failed = "this didn't work";
            }

            List<Venue> venueList = dao.GetCategories(venId);
            foreach (Venue venue in venueList)
            {
                if (venue.Category == "test")
                {
                    result = true;
                    break;
                }
            }
            Assert.IsTrue(result);
        }
    }
}
