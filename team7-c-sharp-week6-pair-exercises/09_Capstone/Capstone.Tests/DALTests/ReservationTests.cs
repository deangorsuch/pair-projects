﻿using Capstone.DAL;
using Capstone.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Transactions;

namespace Capstone.Tests.DALTests
{
    [TestClass]
    public class ReservationTests
    {
        protected string ConnectionString { get; } = "Data Source=.\\sqlexpress;Initial Catalog=excelsior_venues;Integrated Security=True";

        private TransactionScope transaction;

        [TestInitialize]
        public void Setup()
        {
            transaction = new TransactionScope();
        }

        [TestCleanup]
        public void Cleanup()
        {
            transaction.Dispose();
        }
        [TestMethod]
        public void AddReservationAddsAReservation()
        {
            ReservationDAO dao = new ReservationDAO(ConnectionString);
            int spaceId = 0;
            int venId = 0;

            int initialCount = GetRowCount("reservation");

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("INSERT INTO venue (name, city_id, description) VALUES('testVenue', 1, 'this is a test') SELECT SCOPE_IDENTITY();", conn);
                    venId = Convert.ToInt32(cmd.ExecuteScalar());

                    cmd = new SqlCommand("INSERT INTO space (venue_id, name, is_accessible, daily_rate, max_occupancy) VALUES(@venId, 'testSpace', 1, 1.00, 1) SELECT SCOPE_IDENTITY()", conn);
                    cmd.Parameters.AddWithValue("@venid", venId);
                    spaceId = Convert.ToInt32(cmd.ExecuteScalar());
                }

            }
            catch (Exception ex)
            {
                string failed = "this didn't work";
            }

            Reservation reservation = new Reservation();
            reservation.SpaceId = spaceId;
            reservation.NumberOfAttendees = 1;
            reservation.ReservedFor = "testfamily";
            reservation.StartDate = DateTime.Now;
            reservation.EndDate = DateTime.Now;

            dao.AddReservation(reservation);

            int afterAdded = GetRowCount("reservation");

            Assert.AreEqual(initialCount + 1, afterAdded);

        }

        protected int GetRowCount(string table)
        {
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand($"SELECT COUNT(*) FROM {table}", conn); //get # rows after add city
                int count = Convert.ToInt32(cmd.ExecuteScalar());
                return count;
            }
        }
    }
}
