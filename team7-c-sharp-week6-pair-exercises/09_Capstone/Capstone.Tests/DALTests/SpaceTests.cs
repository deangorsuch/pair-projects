﻿using Capstone.DAL;
using Capstone.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Transactions;

namespace Capstone.Tests.DALTests
{
    [TestClass]
    public class SpaceTests
    {
        protected string ConnectionString { get; } = "Data Source=.\\sqlexpress;Initial Catalog=excelsior_venues;Integrated Security=True;";

        private TransactionScope transaction;

        [TestInitialize]
        public void Setup()
        {
            transaction = new TransactionScope();
        }

        [TestCleanup]
        public void Cleanup()
        {
            transaction.Dispose();
        }
        [TestMethod]
        public void GetVenueSpacesGetsANewSpace()
        {
            SpaceDAO dao = new SpaceDAO(ConnectionString);
            Space space = new Space();
            int venId = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {

                    conn.Open();
                    SqlCommand cmd = new SqlCommand("INSERT INTO venue (name, city_id, description) VALUES('testVenue', 1, 'this is a test') SELECT SCOPE_IDENTITY();", conn);
                    venId = Convert.ToInt32(cmd.ExecuteScalar());

                    cmd = new SqlCommand("INSERT INTO space (venue_id, name, is_accessible, open_from, open_to, daily_rate, max_occupancy) VALUES(@venid, 'testtest', 1, 5, 7, 300.00, 78903);", conn);

                    cmd.Parameters.AddWithValue("@venid", venId);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                string failed = "this didn't work";
            }

            List<Space> spaces = dao.GetVenueSpaces(venId);
            bool foundOurSpace = false;

            foreach (Space spaceToFind in spaces)
            {
                if (spaceToFind.Name == "testtest")
                {
                    foundOurSpace = true;
                }
            }

            Assert.IsTrue(foundOurSpace);
        }

        [TestMethod]
        public void GetReservedSpaceReturnsReservedSpace()
        {
            SpaceDAO spaceDAO = new SpaceDAO(ConnectionString);
            ReservationDAO resDAO = new ReservationDAO(ConnectionString);
            int spaceId = 0;
            int venId = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("INSERT INTO venue (name, city_id, description) VALUES('testVenue', 1, 'this is a test') SELECT SCOPE_IDENTITY();", conn);
                    venId = Convert.ToInt32(cmd.ExecuteScalar());

                    cmd = new SqlCommand("INSERT INTO space (venue_id, name, is_accessible, daily_rate, max_occupancy) VALUES(@venId, 'testSpace', 1, 1.00, 1) SELECT SCOPE_IDENTITY()", conn);
                    cmd.Parameters.AddWithValue("@venid", venId);
                    spaceId = Convert.ToInt32(cmd.ExecuteScalar());
                }

            }
            catch (Exception ex)
            {
                string failed = "this didn't work";
            }

            Reservation reservation = new Reservation();
            reservation.NumberOfAttendees = 1;
            reservation.ReservedFor = "testfamily";
            reservation.StartDate = DateTime.Now;
            reservation.EndDate = DateTime.Now;
            reservation.SpaceId = spaceId;

            Space testSpace = spaceDAO.GetReservedSpace(reservation);

            Assert.AreEqual("testSpace", testSpace.Name);
        }

        [TestMethod]
        public void GetAvailableSpaceFindsAvailableSpace()
        {
            SpaceDAO spaceDAO = new SpaceDAO(ConnectionString);
            ReservationDAO resDAO = new ReservationDAO(ConnectionString);
            int spaceId = 0;
            int venId = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("INSERT INTO venue (name, city_id, description) VALUES('testVenue', 1, 'this is a test') SELECT SCOPE_IDENTITY();", conn);
                    venId = Convert.ToInt32(cmd.ExecuteScalar());

                    cmd = new SqlCommand("INSERT INTO space (venue_id, name, is_accessible, daily_rate, max_occupancy) VALUES(@venId, 'testSpace', 1, 1.00, 1) SELECT SCOPE_IDENTITY()", conn);
                    cmd.Parameters.AddWithValue("@venid", venId);
                    spaceId = Convert.ToInt32(cmd.ExecuteScalar());
                }

            }
            catch (Exception ex)
            {
                string failed = "this didn't work";
            }

            Reservation reservation = new Reservation();
            reservation.NumberOfAttendees = 1;
            reservation.ReservedFor = "testfamily";
            reservation.StartDate = DateTime.Now;
            reservation.EndDate = DateTime.Now;

            List<Space> testSpaces = spaceDAO.GetAvailableSpaces(reservation, venId);
            bool spaceFound = false;
            foreach (Space space in testSpaces)
            {
                if (space.Name == "testSpace")
                {
                    spaceFound = true;
                }
            }

            Assert.IsTrue(spaceFound);
        }

        [TestMethod]
        public void GetAvailableSpaceHasTooManyPeople()
        {
            SpaceDAO spaceDAO = new SpaceDAO(ConnectionString);
            int spaceId = 0;
            int venId = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("INSERT INTO venue (name, city_id, description) VALUES('testVenue', 1, 'this is a test') SELECT SCOPE_IDENTITY();", conn);
                    venId = Convert.ToInt32(cmd.ExecuteScalar());

                    cmd = new SqlCommand("INSERT INTO space (venue_id, name, is_accessible, daily_rate, max_occupancy) VALUES(@venId, 'testSpace', 1, 1.00, 1) SELECT SCOPE_IDENTITY()", conn);
                    cmd.Parameters.AddWithValue("@venid", venId);
                    spaceId = Convert.ToInt32(cmd.ExecuteScalar());
                }

            }
            catch (Exception ex)
            {
                string failed = "this didn't work";
            }

            Reservation reservation = new Reservation();
            reservation.NumberOfAttendees = 4;
            reservation.ReservedFor = "testfamily";
            reservation.StartDate = DateTime.Now;
            reservation.EndDate = DateTime.Now;

            List<Space> testSpaces = spaceDAO.GetAvailableSpaces(reservation, venId);
            bool spaceFound = false;
            foreach (Space space in testSpaces)
            {
                if (space.Name == "testSpace")
                {
                    spaceFound = true;
                }
            }

            Assert.IsFalse(spaceFound);
        }

        [TestMethod]
        public void GetAvailableNewResStartsBeforeBookedAndEndsAfterBookedReservation()
        {
            SpaceDAO spaceDAO = new SpaceDAO(ConnectionString);
            ReservationDAO resDAO = new ReservationDAO(ConnectionString);
            int spaceId = 0;
            int venId = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("INSERT INTO venue (name, city_id, description) VALUES('testVenue', 1, 'this is a test') SELECT SCOPE_IDENTITY();", conn);
                    venId = Convert.ToInt32(cmd.ExecuteScalar());

                    cmd = new SqlCommand("INSERT INTO space (venue_id, name, is_accessible, daily_rate, max_occupancy) VALUES(@venId, 'testSpace', 1, 1.00, 1) SELECT SCOPE_IDENTITY()", conn);
                    cmd.Parameters.AddWithValue("@venid", venId);
                    spaceId = Convert.ToInt32(cmd.ExecuteScalar());
                }

            }
            catch (Exception ex)
            {
                string failed = "this didn't work";
            }

            Reservation bookedReservation = new Reservation();
            bookedReservation.SpaceId = spaceId;
            bookedReservation.NumberOfAttendees = 1;
            bookedReservation.ReservedFor = "testfamily";
            bookedReservation.StartDate = Convert.ToDateTime("10/27/2019");
            bookedReservation.EndDate = Convert.ToDateTime("10/28/2019");
            resDAO.AddReservation(bookedReservation);

            Reservation testReservation = new Reservation();
            testReservation.NumberOfAttendees = 1;
            testReservation.StartDate = Convert.ToDateTime("10/26/2019");
            testReservation.EndDate = Convert.ToDateTime("10/29/2019");


            List<Space> testSpaces = spaceDAO.GetAvailableSpaces(testReservation, venId);
            bool spaceFound = false;
            foreach (Space space in testSpaces)
            {
                if (space.Name == "testSpace")
                {
                    spaceFound = true;
                }
            }

            Assert.IsFalse(spaceFound);
        }

        [TestMethod]
        public void GetAvailableStartsDuringBookedButBeforeBookedEndsEndsAfterBookedEnds()
        {
            SpaceDAO spaceDAO = new SpaceDAO(ConnectionString);
            ReservationDAO resDAO = new ReservationDAO(ConnectionString);
            int spaceId = 0;
            int venId = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("INSERT INTO venue (name, city_id, description) VALUES('testVenue', 1, 'this is a test') SELECT SCOPE_IDENTITY();", conn);
                    venId = Convert.ToInt32(cmd.ExecuteScalar());

                    cmd = new SqlCommand("INSERT INTO space (venue_id, name, is_accessible, daily_rate, max_occupancy) VALUES(@venId, 'testSpace', 1, 1.00, 1) SELECT SCOPE_IDENTITY()", conn);
                    cmd.Parameters.AddWithValue("@venid", venId);
                    spaceId = Convert.ToInt32(cmd.ExecuteScalar());
                }

            }
            catch (Exception ex)
            {
                string failed = "this didn't work";
            }

            Reservation bookedReservation = new Reservation();
            bookedReservation.SpaceId = spaceId;
            bookedReservation.NumberOfAttendees = 1;
            bookedReservation.ReservedFor = "testfamily";
            bookedReservation.StartDate = Convert.ToDateTime("10/26/2019");
            bookedReservation.EndDate = Convert.ToDateTime("10/28/2019");
            resDAO.AddReservation(bookedReservation);

            Reservation testReservation = new Reservation();
            testReservation.NumberOfAttendees = 1;
            testReservation.StartDate = Convert.ToDateTime("10/27/2019");
            testReservation.EndDate = Convert.ToDateTime("10/29/2019");


            List<Space> testSpaces = spaceDAO.GetAvailableSpaces(testReservation, venId);
            bool spaceFound = false;
            foreach (Space space in testSpaces)
            {
                if (space.Name == "testSpace")
                {
                    spaceFound = true;
                }
            }

            Assert.IsFalse(spaceFound);
        }

        [TestMethod]
        public void GetAvailableStartsDuringBookedAndEndsDuringBooked()
        {
            SpaceDAO spaceDAO = new SpaceDAO(ConnectionString);
            ReservationDAO resDAO = new ReservationDAO(ConnectionString);
            int spaceId = 0;
            int venId = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("INSERT INTO venue (name, city_id, description) VALUES('testVenue', 1, 'this is a test') SELECT SCOPE_IDENTITY();", conn);
                    venId = Convert.ToInt32(cmd.ExecuteScalar());

                    cmd = new SqlCommand("INSERT INTO space (venue_id, name, is_accessible, daily_rate, max_occupancy) VALUES(@venId, 'testSpace', 1, 1.00, 1) SELECT SCOPE_IDENTITY()", conn);
                    cmd.Parameters.AddWithValue("@venid", venId);
                    spaceId = Convert.ToInt32(cmd.ExecuteScalar());
                }

            }
            catch (Exception ex)
            {
                string failed = "this didn't work";
            }

            Reservation bookedReservation = new Reservation();
            bookedReservation.SpaceId = spaceId;
            bookedReservation.NumberOfAttendees = 1;
            bookedReservation.ReservedFor = "testfamily";
            bookedReservation.StartDate = Convert.ToDateTime("10/26/2019");
            bookedReservation.EndDate = Convert.ToDateTime("10/29/2019");
            resDAO.AddReservation(bookedReservation);

            Reservation testReservation = new Reservation();
            testReservation.NumberOfAttendees = 1;
            testReservation.StartDate = Convert.ToDateTime("10/27/2019");
            testReservation.EndDate = Convert.ToDateTime("10/28/2019");


            List<Space> testSpaces = spaceDAO.GetAvailableSpaces(testReservation, venId);
            bool spaceFound = false;
            foreach (Space space in testSpaces)
            {
                if (space.Name == "testSpace")
                {
                    spaceFound = true;
                }
            }

            Assert.IsFalse(spaceFound);
        }

        [TestMethod]
        public void GetAvailableStartBeforeBookedAndEndsDuringReservation()
        {
            SpaceDAO spaceDAO = new SpaceDAO(ConnectionString);
            ReservationDAO resDAO = new ReservationDAO(ConnectionString);
            int spaceId = 0;
            int venId = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("INSERT INTO venue (name, city_id, description) VALUES('testVenue', 1, 'this is a test') SELECT SCOPE_IDENTITY();", conn);
                    venId = Convert.ToInt32(cmd.ExecuteScalar());

                    cmd = new SqlCommand("INSERT INTO space (venue_id, name, is_accessible, daily_rate, max_occupancy) VALUES(@venId, 'testSpace', 1, 1.00, 1) SELECT SCOPE_IDENTITY()", conn);
                    cmd.Parameters.AddWithValue("@venid", venId);
                    spaceId = Convert.ToInt32(cmd.ExecuteScalar());
                }

            }
            catch (Exception ex)
            {
                string failed = "this didn't work";
            }

            Reservation bookedReservation = new Reservation();
            bookedReservation.SpaceId = spaceId;
            bookedReservation.NumberOfAttendees = 1;
            bookedReservation.ReservedFor = "testfamily";
            bookedReservation.StartDate = Convert.ToDateTime("10/26/2019");
            bookedReservation.EndDate = Convert.ToDateTime("10/29/2019");
            resDAO.AddReservation(bookedReservation);

            Reservation testReservation = new Reservation();
            testReservation.NumberOfAttendees = 1;
            testReservation.StartDate = Convert.ToDateTime("10/25/2019");
            testReservation.EndDate = Convert.ToDateTime("10/28/2019");


            List<Space> testSpaces = spaceDAO.GetAvailableSpaces(testReservation, venId);
            bool spaceFound = false;
            foreach (Space space in testSpaces)
            {
                if (space.Name == "testSpace")
                {
                    spaceFound = true;
                }
            }

            Assert.IsFalse(spaceFound);
        }

        [TestMethod]
        public void GetAvailableNonconflictingReservations()
        {
            SpaceDAO spaceDAO = new SpaceDAO(ConnectionString);
            ReservationDAO resDAO = new ReservationDAO(ConnectionString);
            int spaceId = 0;
            int venId = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("INSERT INTO venue (name, city_id, description) VALUES('testVenue', 1, 'this is a test') SELECT SCOPE_IDENTITY();", conn);
                    venId = Convert.ToInt32(cmd.ExecuteScalar());

                    cmd = new SqlCommand("INSERT INTO space (venue_id, name, is_accessible, daily_rate, max_occupancy) VALUES(@venId, 'testSpace', 1, 1.00, 1) SELECT SCOPE_IDENTITY()", conn);
                    cmd.Parameters.AddWithValue("@venid", venId);
                    spaceId = Convert.ToInt32(cmd.ExecuteScalar());
                }
            }
            catch (Exception ex)
            {
                string failed = "this didn't work";
            }

            Reservation bookedReservation = new Reservation();
            bookedReservation.SpaceId = spaceId;
            bookedReservation.NumberOfAttendees = 1;
            bookedReservation.ReservedFor = "testfamily";
            bookedReservation.StartDate = Convert.ToDateTime("10/26/2019");
            bookedReservation.EndDate = Convert.ToDateTime("10/29/2019");
            resDAO.AddReservation(bookedReservation);

            Reservation testReservation = new Reservation();
            testReservation.NumberOfAttendees = 1;
            testReservation.StartDate = Convert.ToDateTime("10/30/2019");
            testReservation.EndDate = Convert.ToDateTime("10/31/2019");


            List<Space> testSpaces = spaceDAO.GetAvailableSpaces(testReservation, venId);
            bool spaceFound = false;
            foreach (Space space in testSpaces)
            {
                if (space.Name == "testSpace")
                {
                    spaceFound = true;
                }
            }

            Assert.IsTrue(spaceFound);
        }
    }
}



