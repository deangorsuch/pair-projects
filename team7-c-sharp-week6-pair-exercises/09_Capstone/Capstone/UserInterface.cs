﻿using Capstone.DAL;
using Capstone.Models;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Capstone
{
    public class UserInterface


    {
        private IVenueDAO venueDAO;
        private ISpaceDAO spaceDAO;
        private IReservationDAO reservationDAO;
        private Venue selectedVenue;

        public UserInterface(string connectionString)
        {
            venueDAO = new VenueDAO(connectionString);
            spaceDAO = new SpaceDAO(connectionString);
            reservationDAO = new ReservationDAO(connectionString);
        }

        public void Run()
        {
            PrintMainMenu();
            bool done = false;
            string mainMenuSelection = Console.ReadLine();

            while (!done)
            {
                switch (mainMenuSelection.ToLower())
                {
                    case "1":
                        NavigateVenues();
                        break;
                    case "q":
                        done = true;
                        return;
                    default:
                        Console.WriteLine("Please make a valid selection.");
                        break;
                }

                PrintMainMenu();
                mainMenuSelection = Console.ReadLine();
            }

        }

        private void PrintMainMenu()
        {
            Console.WriteLine("Excelsior Venues: MAIN MENU ");
            Console.WriteLine("Press 1 to see a list of our venues ");
            Console.WriteLine("Press Q to exit this menu ");
        }

        private void PrintAllVenues()
        {
            Console.WriteLine("Which venue would you like to view? ");
            GetVenues();
            Console.WriteLine("Choose R to return to Previous Screen ");
        }

        private void NavigateVenues()
        {
            PrintAllVenues();
            string selection = Console.ReadLine();
            bool done = false;
            List<Venue> venues = venueDAO.GetAllVenues();
            int selectionIndex = 0;

            while (!done)
            {
                string message = venueDAO.SelectionOK(selection, venues);
                if (selection.ToLower() == "r")
                {
                    done = true;
                    return;
                }

                if (message.Length > 0)
                {
                    Console.WriteLine();
                    Console.WriteLine(message);
                    Console.WriteLine();
                    NavigateVenues();
                    selection = Console.ReadLine();
                    break;
                }

                if (int.TryParse(selection, out selectionIndex))
                {
                    selectionIndex = int.Parse(selection) - 1;
                    selectedVenue = venues[selectionIndex];
                    selectedVenue = venueDAO.GetSpecificVenue(selectedVenue.VenueId);

                    Console.WriteLine(selectedVenue.Name);
                    Console.WriteLine("Location: " + selectedVenue.CityName + ", " + selectedVenue.State);
                    Console.WriteLine("Categories: ");

                    List<Venue> categories = venueDAO.GetCategories(selectedVenue.VenueId);
                    for (int i = 0; i < categories.Count; i++)
                    {
                        Console.WriteLine(categories[i].Category);
                    }
                    Console.WriteLine();
                    Console.WriteLine(selectedVenue.Description);
                    Console.WriteLine();

                    NavigateVenueDetails();
                    break;
                }
            }
            NavigateVenues();
        }

        private void DisplayVenueDetails() //when a specific venue is chosen
        {
            Console.WriteLine("What would you like to do next? ");
            Console.WriteLine("Choose 1 to view space ");
            Console.WriteLine("Choose 2 to search for reservation ");
            Console.WriteLine("Choose R to return to previous screen ");
        }

        private void NavigateVenueDetails()
        {
            DisplayVenueDetails();
            string selection = Console.ReadLine();
            bool done = false;
            while (!done)
            {
                switch (selection.ToLower())
                {
                    case "1":

                        //ListVenueSpaces();
                        NavigateSpaces();
                        break;

                    case "2":
                        Console.WriteLine("This feature is still in development. Come back later for our exciting new updates!");
                        Console.WriteLine();
                        break;
                    case "r":
                        done = true;
                        return;
                    default:
                        Console.WriteLine("Please make a valid selection.");
                        break;
                }

                DisplayVenueDetails();
                selection = Console.ReadLine();
            }
        }

        private void ListVenueSpaces() //after 1 is chosen above
        {
            List<Space> spaceList = new List<Space>();
            spaceList = spaceDAO.GetVenueSpaces(selectedVenue.VenueId);
            //display all venue spaces
            Console.WriteLine("Name ".PadLeft(15) + "Open ".PadLeft(35) + "Close ".PadLeft(10) + "Daily Rate ".PadLeft(20) + "Max Occupancy ".PadLeft(20));


            for (int i = 0; i < spaceList.Count; i++)
            {
                Console.Write("#" + (i + 1).ToString().PadRight(5) + spaceList[i].Name.PadRight(40) + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(spaceList[i].OpenFrom).PadRight(10) + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(spaceList[i].OpenTo).PadRight(15) + spaceList[i].DailyRate.ToString("C2").PadRight(10) + spaceList[i].MaxOccupancy.ToString().PadLeft(15));

                Console.WriteLine();

            }

            Console.WriteLine();
            Console.WriteLine("What would you like to do next? ");
            Console.WriteLine("Press 1 to reserve a space ");
            Console.WriteLine("Press R to return to previous screen ");
        }


        private void NavigateSpaces()
        {
            ListVenueSpaces();
            string selection = Console.ReadLine();
            bool done = false;

            while (!done)
            {
                switch (selection.ToLower())
                {
                    case "1":
                        ReserveASpace();
                        done = true;
                        return;

                    case "r":
                        done = true;
                        return;

                    default:
                        Console.WriteLine("Please make a valid selection.");
                        break;
                }
                selection = Console.ReadLine();
            }
        }

        private void ReserveASpace()
        {
            Reservation reservation = new Reservation();
            Console.WriteLine("When do you need the space? DD/MM/YYYY");
            try
            {
                reservation.StartDate = Convert.ToDateTime(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Please use the correct date format. Returning to previous menu.");
                Console.WriteLine();
                return;
            }
            Console.WriteLine("How many days will you need the space? ");
            double reservationDays = 0;
            try
            {
                reservationDays = Convert.ToDouble(Console.ReadLine());
                reservation.EndDate = reservation.StartDate.AddDays(reservationDays);
            }
            catch (FormatException)
            {
                Console.WriteLine("Please enter a number. Returning to previous menu.");
                Console.WriteLine();
                return;
            }

            Console.WriteLine("How many people will be in attendance? ");
            try
            {
                reservation.NumberOfAttendees = Convert.ToInt32(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Please enter a number. Returning to previous menu.");
                Console.WriteLine();
                return;
            }

            Console.WriteLine("The following spaces are available based on your needs: ");

            List<Space> availableSpace = spaceDAO.GetAvailableSpaces(reservation, selectedVenue.VenueId);
            if (availableSpace.Count == 0)
            {
                SearchFailed();
                return;
            }
            Console.WriteLine("Space # " + "Name ".PadLeft(15) + "Daily Rate ".PadLeft(34) + "Accessible? ".PadLeft(17) + "Total Cost".PadLeft(15));
            //display list of spaces

            for (int i = 0; i < availableSpace.Count; i++)
            {
                Console.Write(availableSpace[i].Id.ToString().PadRight(10) + availableSpace[i].Name.PadRight(40).ToString().PadRight(10) + availableSpace[i].MaxOccupancy.ToString().PadRight(15) + availableSpace[i].accessibleToDescr(availableSpace[i].IsAccessible).PadRight(10) + "     " + (availableSpace[i].DailyRate * Convert.ToDecimal(reservationDays)).ToString("C2"));

                Console.WriteLine();
            }
            Console.WriteLine();
            Console.WriteLine("Which space would you like to reserve (enter 0 to cancel)? ");
            string selectedSpace = Console.ReadLine();
            string spaceOK = spaceDAO.CheckSpaces(availableSpace, selectedSpace);

            if (spaceOK.Length > 0)
            {
                Console.WriteLine();
                Console.WriteLine(spaceOK);
                Console.WriteLine("Returning to previous menu");
                Console.WriteLine();
                return;
            }

            if (Convert.ToInt32(selectedSpace) == 0)
            {
                return;
            }

            reservation.SpaceId = Convert.ToInt32(selectedSpace);

            Console.WriteLine("Who is this reservation for? ");
            reservation.ReservedFor = Console.ReadLine();
            reservation = reservationDAO.AddReservation(reservation);
            Space reservedSpace = new Space();
            reservedSpace = spaceDAO.GetReservedSpace(reservation);
            Console.WriteLine("Thanks for submitting your reservation! The details for your event are listed below: ");
            Console.WriteLine("Confirmation #: " + reservation.ReservationId);
            Console.WriteLine("Venue: " + selectedVenue.Name);//insert venue info
            Console.WriteLine("Space: " + reservedSpace.Name);//space info
            Console.WriteLine("Reserved for: " + reservation.ReservedFor); //insert person name/group
            Console.WriteLine("Attendees: " + reservation.NumberOfAttendees);//insert user input for # of occupancys
            Console.WriteLine("Arrival Date: " + reservation.StartDate);
            Console.WriteLine("Depart date: " + reservation.EndDate);
            Console.WriteLine("Total Cost: " + (reservedSpace.DailyRate * Convert.ToDecimal(reservationDays)).ToString("C2"));
            Console.WriteLine();
            return;
        }


        private void GetVenues()
        {
            List<Venue> venues = venueDAO.GetAllVenues();

            for (int i = 0; i < venues.Count; i++)
            {
                Console.WriteLine((i + 1) + ") " + venues[i].Name);
            }
        }

        private void SearchFailed()
        {
            Console.WriteLine("No Available Spaces. Would you like to try again? Y/N");
            string selection = Console.ReadLine();
            bool done = false;

            while (!done)
            {
                switch (selection.ToLower())
                {
                    case "y":
                        ReserveASpace();
                        break;
                    case "n":
                        done = true;
                        return;
                    default:
                        Console.WriteLine("Please enter a valid command.");
                        SearchFailed();
                        break;
                }

            }
        }
    }
}