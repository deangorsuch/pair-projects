﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Capstone.Models
{
    public class Reservation
    {
        public int SpaceId { get; set; }
        public int ReservationId { get; set; }
        public string ReservedFor { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int NumberOfAttendees { get; set; }

        public override string ToString()
        {
            return ReservationId.ToString().PadRight(10) + SpaceId.ToString().PadRight(10) + NumberOfAttendees.ToString().PadRight(10) + StartDate.ToString().PadRight(10) + EndDate.ToString().PadRight(10) + ReservedFor.PadRight(10);
        }
    }
}
