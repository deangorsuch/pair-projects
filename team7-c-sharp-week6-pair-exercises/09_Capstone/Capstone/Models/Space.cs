﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Capstone.Models
{
    public class Space
    {
        public int Id { get; set; }
        public int VenueId { get; set; }
        public string Name { get; set; }
        public bool IsAccessible { get; set; }
        public int OpenFrom { get; set; }
        public int OpenTo { get; set; }
        public decimal DailyRate { get; set; }
        public int MaxOccupancy { get; set; }

        public string accessibleToDescr(bool accessible)
        {
            string retString = string.Empty;
            if (accessible == true)
            {
                retString = "Yes";
            }
            else if (accessible == false)
            {
                retString = "No";
            }
  
            return retString;
        }

    }
}
