﻿namespace Capstone.Models
{
    public class Venue
    {
        public int VenueId { get; set; }
        public string Name { get; set; }
        public int CityId { get; set; }
        public string Description { get; set; }
        public string CityName { get; set; }
        public string State { get; set; }
        public string Category { get; set; }

        public override string ToString()
        {
            return VenueId.ToString().PadRight(10) + Name.PadRight(10) + CityId.ToString().PadRight(10) + Description.PadRight(10);
        }
    }
}
