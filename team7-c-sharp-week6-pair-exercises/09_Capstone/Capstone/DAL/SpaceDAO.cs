﻿using Capstone.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Capstone.DAL
{
    public class SpaceDAO : ISpaceDAO
    {
        private string connectionString;


        public SpaceDAO(string databaseconnectionString)
        {
            connectionString = databaseconnectionString;
        }

        public List<Space> GetVenueSpaces(int venueId)
        {
            List<Space> spaces = new List<Space>();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT * FROM space WHERE venue_id = @venueId;", conn);
                    cmd.Parameters.AddWithValue("@venueId", venueId);

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Space space = ConvertReaderToSpace(reader);
                        spaces.Add(space);
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("An error occurred communicating with the database. ");
                Console.WriteLine(ex.Message);
                throw;
            }

            return spaces;
        }

        public Space GetReservedSpace(Reservation reservation)
        {
            Space userSpace = new Space();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT * FROM space WHERE id = @spaceid;", conn);
                    cmd.Parameters.AddWithValue("@spaceid", reservation.SpaceId);

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        userSpace = ConvertReaderToSpace(reader);
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("An error occurred communicating with the database. ");
                Console.WriteLine(ex.Message);
                throw;
            }
            return userSpace;
        }

        public List<Space> GetAvailableSpaces(Reservation reservation, int venueId)
        {
            List<Space> availableSpaces = new List<Space>();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT TOP 5 * FROM space s where venue_id = @venue_id AND s.max_occupancy >= @attendees AND s.id NOT IN " +
                    "(SELECT s.id from reservation r JOIN space s on r.space_id = s.id " +
                    "WHERE s.venue_id = @venue_id AND r.end_date >= @req_from_date AND r.start_date <= @req_to_date)", conn);

                    cmd.Parameters.AddWithValue("@venue_id", venueId);
                    cmd.Parameters.AddWithValue("@req_from_date", reservation.StartDate);
                    cmd.Parameters.AddWithValue("@req_to_date", reservation.EndDate);
                    cmd.Parameters.AddWithValue("@attendees", reservation.NumberOfAttendees);

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Space space = new Space();

                        space = ConvertReaderToSpace(reader);
                        availableSpaces.Add(space);
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("An error occurred communicating with the database. ");
                Console.WriteLine(ex.Message);
                throw;
            }
            return availableSpaces;
        }

        public string CheckSpaces(List<Space> availablespaces, string selection)
        {
            string spaceOK = "";
            int selectionIndex = 0;
            bool inList = false;
            if (int.TryParse(selection, out selectionIndex) == false)
            {
                spaceOK = "Please make a valid selection";
            }

            if (int.TryParse(selection, out selectionIndex) == true)
            {
                foreach (Space space in availablespaces)
                {
                    if (selectionIndex == space.Id)
                    {
                        inList = true;
                    }
                }
                if (!inList)
                {
                    spaceOK = "That is an invalid space ID.";
                }
            }
            return spaceOK;
        }

        private Space ConvertReaderToSpace(SqlDataReader reader)
        {
            Space space = new Space();

            space.VenueId = Convert.ToInt32(reader["venue_id"]);
            space.Id = Convert.ToInt32(reader["id"]);
            space.Name = Convert.ToString(reader["name"]);
            space.IsAccessible = Convert.ToBoolean(reader["is_accessible"]);
            if (reader["open_from"] is DBNull)
            {
                space.OpenFrom = 1;
            }
            else
            {
                space.OpenFrom = Convert.ToInt32(reader["open_from"]);
            }
            if (reader["open_to"] is DBNull)
            {
                space.OpenTo = 12;
            }
            else
            {
                space.OpenTo = Convert.ToInt32(reader["open_to"]);
            }

            space.DailyRate = Convert.ToDecimal(reader["daily_rate"]);
            space.MaxOccupancy = Convert.ToInt32(reader["max_occupancy"]);

            return space;
        }
    }
}

