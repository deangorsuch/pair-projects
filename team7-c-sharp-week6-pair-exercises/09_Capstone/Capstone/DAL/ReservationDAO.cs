﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using Capstone.Models;

namespace Capstone.DAL
{
    //after space is selected a reservation can be made
    public class ReservationDAO : IReservationDAO
    {
        private string connectionString;
        private string sql_AddReservation = "INSERT INTO reservation (space_id, number_of_attendees, start_date, end_date, reserved_for) "
            + "VALUES(@space_id, @attendees, @start_date, @end_date, @reservedFor)";

        public ReservationDAO(string databaseconnectionString)
        {
            connectionString = databaseconnectionString;
        }

        public Reservation AddReservation(Reservation newReservation)
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql_AddReservation, conn))
                    {

                        cmd.Parameters.AddWithValue("@space_id", newReservation.SpaceId);
                        cmd.Parameters.AddWithValue("@start_date", newReservation.StartDate.ToString("yyyy-MM-dd"));
                        cmd.Parameters.AddWithValue("@end_date", newReservation.EndDate.ToString("yyyy-MM-dd"));
                        cmd.Parameters.AddWithValue("@attendees", newReservation.NumberOfAttendees);
                        cmd.Parameters.AddWithValue("@reservedFor", newReservation.ReservedFor);

                        cmd.ExecuteNonQuery();
                    }

                    using (SqlCommand cmd = new SqlCommand("SELECT * FROM reservation WHERE space_id = @space_id AND @start_date = @start_date AND end_date = @end_date AND reserved_for = @reservedFor", conn))
                    {
                        cmd.Parameters.AddWithValue("@space_id", newReservation.SpaceId);
                        cmd.Parameters.AddWithValue("@start_date", newReservation.StartDate.ToString("yyyy-MM-dd"));
                        cmd.Parameters.AddWithValue("@end_date", newReservation.EndDate.ToString("yyyy-MM-dd"));
                        cmd.Parameters.AddWithValue("@attendees", newReservation.NumberOfAttendees);
                        cmd.Parameters.AddWithValue("@reservedFor", newReservation.ReservedFor);

                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            newReservation.ReservationId = Convert.ToInt32(reader["reservation_id"]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return newReservation;
        }
    }
}
