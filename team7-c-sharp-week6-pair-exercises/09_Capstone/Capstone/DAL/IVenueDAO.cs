﻿using Capstone.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Capstone.DAL
{
    public interface IVenueDAO
    {
        List<Venue> GetAllVenues();

        Venue GetSpecificVenue(int venueID);

        List<Venue> GetCategories(int venueID);

        string SelectionOK(string selection, List<Venue> venues);
    }
}
