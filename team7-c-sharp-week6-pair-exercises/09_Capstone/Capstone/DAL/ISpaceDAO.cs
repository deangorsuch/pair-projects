﻿using Capstone.Models;
using System.Collections.Generic;

namespace Capstone.DAL
{
    public interface ISpaceDAO
    {

        List<Space> GetVenueSpaces(int venueId);

        Space GetReservedSpace(Reservation reservation);

        List<Space> GetAvailableSpaces(Reservation reservation, int venueId);

        string CheckSpaces(List<Space> availablespaces, string selection);
    }
}
