﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using Capstone.Models;

namespace Capstone.DAL
{
    public class VenueDAO : IVenueDAO
    {
        private string connectionString;

        public VenueDAO(string databaseconnectionString)
        {
            connectionString = databaseconnectionString;
        }

        public List<Venue> GetAllVenues()
        {
            List<Venue> venues = new List<Venue>();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT * FROM venue ORDER BY name;", conn);

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Venue venue = ConvertReaderToVenue(reader);
                        venues.Add(venue);
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("An error occurred communicating with the database. ");
                Console.WriteLine(ex.Message);
                throw;
            }

            return venues;
        }

        public Venue GetSpecificVenue(int venueID)
        {
            Venue userVenue = new Venue();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT venue.id AS venueid, venue.name AS venuename, description, city.name AS cityname, state_abbreviation FROM venue JOIN city ON city.id = venue.city_id " +
                    "WHERE venue.id = @id", conn);

                    cmd.Parameters.AddWithValue("@id", venueID);

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        //userVenue = ConvertReaderToVenue(reader);
                        userVenue.Name = Convert.ToString(reader["venuename"]);
                        userVenue.VenueId = Convert.ToInt32(reader["venueid"]);
                        userVenue.Description = Convert.ToString(reader["description"]);
                        userVenue.State = Convert.ToString(reader["state_abbreviation"]);
                        userVenue.CityName = Convert.ToString(reader["cityname"]);

                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("An error occurred communicating with the database. ");
                Console.WriteLine(ex.Message);
                throw;
            }
            return userVenue;
        }

        public List<Venue> GetCategories(int venueID)
        {
            List<Venue> categories = new List<Venue>();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT category.name FROM venue " +
                        "JOIN category_venue ON category_venue.venue_id = venue.id " +
                        "JOIN category ON category_venue.category_id = category.id " +
                        "WHERE venue.id = @id", conn);

                    cmd.Parameters.AddWithValue("@id", venueID);

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Venue venue = new Venue();
                        venue.Category = Convert.ToString(reader["name"]);
                        categories.Add(venue);
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("An error occurred communicating with the database. ");
                Console.WriteLine(ex.Message);
                throw;
            }

            return categories;
        }

        private Venue ConvertReaderToVenue(SqlDataReader reader)
        {
            Venue venue = new Venue();

            venue.VenueId = Convert.ToInt32(reader["id"]);
            venue.Name = Convert.ToString(reader["name"]);
            venue.Description = Convert.ToString(reader["description"]);
            venue.CityId = Convert.ToInt32(reader["city_id"]);

            return venue;
        }

        public string SelectionOK(string selection, List<Venue> venues)
        {
            int selectionIndex = 0;
            string selectionMessage = "";
            if (int.TryParse(selection, out selectionIndex) == false)
            {
                selectionMessage = "Please make a valid selection";
            }
            if (int.TryParse(selection, out selectionIndex) == true && (selectionIndex) > venues.Count)
            {
                selectionMessage = "There are not that many venues. Please enter a valid venue.";

            }
            if (int.TryParse(selection, out selectionIndex) == true && (selectionIndex < 1))
            {
                selectionMessage = "That is an invalid venue number.";
            }
            return selectionMessage;
        }
    }
}
